&nbsp;&nbsp;

## **Introduction**

This document contains information about the Bitcoin Dashboard.


## **Indice**
1. General objective  
2. Architecture  
3. Technical requirements  
4. Running system

**Note:** It is recommend that you open this README in another tab as you perform the tasks below. 


---

## **1. General objective**

The dashboard aims to give a general idea of the behavior and analysis of the cryptocurrency Bitcoin.

---

## **2. Architecture**

The Dashboard is made up of 3 sections:

1. short term: contains the short-term analysis of BTC.
2. medium term: contains the long-term analysis of BTC.
3. definitions: It contains the definition of the terms used as well as other information of general interest.

---

## **3. Technical requirements**

The Dashboard has been developed and tested using the following technologies:

* Operating system: Ubuntu >= 20.04
* R: version 4.0.3

---

## **4. Running system**

You need to compile the btc_investment.Rmd file located in the visualization folder.

The result of a successful compilation should generate a Dashboard like the one shown in the following images.

![BTC Short term](images/btc_short_term.png)
![BTC Medium term](images/btc_medium_term.png)
---

&nbsp;&nbsp;&nbsp;&nbsp;